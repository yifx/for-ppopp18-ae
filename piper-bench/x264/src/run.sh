#!/bin/bash
cilkview="../../../../../piper_cilkview/cilkutil/bin/cilkview"

if [ $# -le 2 ]; then
echo "Usage: ./run.sh <prog> <data size> <output file> <nproc> \"[extra]\" where"
echo "  <prog> can be serial or cilk and "
echo "  <data size> can be tiny, small, medium, large, and native."
exit 0
fi 

prog=$1
size=$2
out=$3
args=("$@")

if [ $# -ge 5 ]; then
count=4  # args is 0-index based
while [[ $count -le $# ]]; do
    echo "${args[$count]}" 
    extra="$extra ${args[$count]}" 
    count=$count+1;
done
else
# set both cilk and serial to use --mvrange 32 to get the same output
# extra="--mvrange 32"
extra=""
fi

# Took away the mvrange; use mvrange-thread instead
if [ $1 = 'cilk' ]; then
    nproc="--nproc $4"
    extra="$extra --mvrange-thread 24"
#    extra="$extra --mvrange-thread 32"
elif [ $1 = 'piper' ]; then
    extra="$extra --mvrange-thread 24"
#    extra="$extra --mvrange-thread 32"
elif [ $1 = 'cilkview' ]; then
    nproc=""
    extra="$extra --mvrange-thread 24"
else 
    nproc=""
    # extra="$extra --mvrange 32"
fi

if [ "$size" = 'tiny' ]; then 
    input="eledream_64x36_3.y4m"
elif [ "$size" = 'small' ]; then  
    input="eledream_640x360_8.y4m"
elif [ "$size" = 'medium' ]; then  
    input="eledream_640x360_32.y4m"
elif [ "$size" = 'large' ]; then  
    input="eledream_640x360_128.y4m"
elif [ "$size" = 'native' ]; then  
    input="eledream_1920x1080_512.y4m"
fi

# original setting: with the --b-pyramid setting, we can have B frame used as a reference; 
# disable this for now
# cmd="./x264-$prog $nproc $extra --quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --b-pyramid --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 -o $out ../data/$input"
# the current cmd to run with 
cmd="./build/$prog/x264-$prog $nproc $extra --quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --bframes 0 -o $out ../data/$input"
# used for debugging; restict number of frames outputted (--frames <int>) and 
# cause the encoded frame to be dumped (--dump-yuv <string>)
# cmd="./x264-$prog $nproc $extra --frame 1 --dump-yuv dump.log --quiet --qp 20 --partitions b8x8,i4x4 --ref 5 --direct auto --weightb --mixed-refs --no-fast-pskip --me umh --subme 7 --analyse b8x8,i4x4 --bframes 0 -o $out ../data/$input"

if [ $1 = 'cilkview' ]; then
cv="$cilkview --debug -- $cmd"
echo "$cv" 
echo `$cv` 
else
echo "$cmd"
echo `$cmd`
fi
