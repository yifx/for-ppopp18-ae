prefix=/usr/local
exec_prefix=${prefix}
bindir=${exec_prefix}/bin
libdir=${exec_prefix}/lib
includedir=${prefix}/include
ARCH=X86_64
SYS=LINUX
CC=gcc
CFLAGS=-O4 -ffast-math  -Wall -I. -DHAVE_CONFIG_H -D_XOPEN_SOURCE=600 -std=gnu99 -DNDEBUG -DHAVE_MALLOC_H -DHAVE_MMX -DARCH_X86_64 -DSYS_LINUX -DHAVE_PTHREAD -s -fomit-frame-pointer
ALTIVECFLAGS=
LDFLAGS= -lrt -lm -lpthread
AS=yasm
ASFLAGS=-f elf -m amd64
EXE=
VIS=no
HAVE_GETOPT_LONG=1
DEVNULL=/dev/null
ECHON=echo -n
CONFIGURE_ARGS= '--extra-cflags=-DHAVE_CONFIG_H' '--extra-cflags=-D_XOPEN_SOURCE=600' '--extra-cflags=-std=gnu99' '--extra-cflags=-DNDEBUG' '--extra-ldflags=-lrt'
