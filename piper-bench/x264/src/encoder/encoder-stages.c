#include "debug.h"

#define PADDING 0

// because it is too long to scroll over every time 
// precondition: all parameters prepared
// postcondition: all parameters changes
// side effects: changes are within parameters, also global polling vars
// return: 0,-1 fail 1 success


static inline void write_padding(int frame_no, hnd_t hout) {

    unsigned char mark[3];
    mark[0] = 85;
    mark[1] = 0;
    mark[2] = frame_no;

    fwrite (mark, 1, 1, hout);
    fwrite (mark, 1, 1, hout);
    fwrite (mark+1, 1, 1, hout);
    fwrite (mark+1, 1, 1, hout);
    fwrite (mark+2, 1, 1, hout);
    int j;
    for(j=0; j<100; j++) {
        fwrite (mark+1, 1, 1, hout);
    }

}

/* ANGE: This is stage 0 --- must check left before calling this function, because 
         this function calls sync_context, reference_update, and reference_reset,
         which updates the frames.unused list that are subsequently copied into 
         the next iteration's context in its own stage 0 */
static inline int 
x264_stage_init(x264_t *h, x264_picture_t *pic_in, int frame_no, 
                x264_picture_t* pic_out, int *pi_global_qp) {

    int     i_nal_type;
    int     i_nal_ref_idc;
    int     i_global_qp;

    if(frame_no != 0) {
        /* ANGE XXX: sync_context, sync_ratecontrol, reference_update, reference_reset 
           push_unused, and update of poc on fdec need to be its own stage w/ check left */ 
        // would the subsequent stages change the context being copies?   JJJ
        x264_thread_sync_context( h, h->pred ); // important J 
        x264_thread_sync_ratecontrol( h, h->pred );  

        // ok to call this before encoding any frames, since the initial 
        // values of fdec have b_kept_as_ref=0
        /* ANGE: note that this calls push and pop unused, which is copied across context */
        x264_reference_update( h );
    }
    
    /* ------------------- Setup new frame from picture -------------------- */
    if( pic_in != NULL ) {

        x264_frame_t *fenc = x264_frame_pop_unused( h );
        // ANGE: Using reference counting fixes this
        // apparently, here is a race, two processors pops the same frame if previous frame is I
        // let's new it 
        // x264_frame_t *fenc = x264_frame_new( h );
        fenc->b_intra_calculated = 0;
        
        if( x264_frame_copy_picture( h, fenc, pic_in ) < 0 ) {   
            fprintf( stderr, "x264 [error]: x264_encoder_encode failed\n" );  //J
            return -1;
        }

        if( h->param.i_width != 16 * h->sps->i_mb_width ||
            h->param.i_height != 16 * h->sps->i_mb_height )
            x264_frame_expand_border_mod16( h, fenc );

        fenc->i_frame = h->frames.i_input++;
        x264_frame_push( h->frames.next, fenc );

        if( h->frames.b_have_lowres )
            x264_frame_init_lowres( h, fenc );

        if( h->param.rc.i_aq_mode )
            x264_adaptive_quant_frame( h, fenc );

        if( h->frames.i_input <= h->frames.i_delay + 1 - h->param.i_max_active_iters ) {
            
            // ANGE: this should not occur if we don't have b frames
            WHEN_DEBUG( assert(0); ) 
            /* Nothing yet to encode */
            /* waiting for filling bframe buffer */
            pic_out->i_type = X264_TYPE_AUTO;
            return 0;
        }
    }

    if( h->frames.current[0] == NULL ) {
        int bframes = 0;
        /* 2: Select frame types */
        assert(h->frames.next[0] != NULL);

        /* ANGE: frame->i_type will be updated in h->frames.next */
        /* ANGE: also seem to update h->mb.i_mb_y ?? */
        x264_slicetype_decide( h );

        /* 3: move some B-frames and 1 non-B to encode queue */
        /* ANGE: shift returns the one at index bframes and shift everthings after
         *       that one to the left; now there is 1 non-B in frames.current */
        while( IS_X264_TYPE_B( h->frames.next[bframes]->i_type ) ) {
            bframes++;
        }
        /* ANGE: we disable B-frames for now so this must be true */
        WHEN_DEBUG( assert(bframes == 0); )

        /* ANGE: shift returns the one at index bframes and shift everthings after
         *       that one to the left; now there is 1 non-B in frames.current */
        x264_frame_push( h->frames.current, x264_frame_shift( &h->frames.next[bframes] ) );
        /* FIXME: when max B-frames > 3, BREF may no longer be centered after GOP closing */
        /* ANGE: a BREF-frame is created in (roughly) the middle of bframes, and that's 
                 processed before all other B frames */
        if( h->param.b_bframe_pyramid && bframes > 1 ) {
            x264_frame_t *mid = x264_frame_shift( &h->frames.next[bframes/2] );
            mid->i_type = X264_TYPE_BREF;
            x264_frame_push( h->frames.current, mid );
            bframes--;
        }
        /* ANGE: Push all the rest of B frames into current after the frames that it reference */
        while( bframes-- )
            x264_frame_push( h->frames.current, x264_frame_shift( h->frames.next ) );
    }

    /* ------------------- Get frame to be encoded ------------------------- */
    /* 4: get picture to encode */
    h->fenc = x264_frame_shift( h->frames.current );
    /* ANGE: this should not happen with the way we are doing things */
    assert(h->fenc != NULL);
       
    /*
    if( h->fenc == NULL ) {
        // Nothing yet to encode (ex: waiting for I/P with B frames) 
        // waiting for filling bframe buffer 
        pic_out->i_type = X264_TYPE_AUTO;
        return 0;
    } */

    if( h->fenc->i_type == X264_TYPE_IDR ) {
        h->frames.i_last_idr = h->fenc->i_frame;
    }

    /* ANGE: the do_encode: label was here originally */

    /* ------------------- Setup frame context ----------------------------- */
    /* 5: Init data dependent of frame type */
    if( h->fenc->i_type == X264_TYPE_IDR ) {
        /* reset ref pictures */
        /* ANGE: note that this calls push and pop unused, which is copied across context */
        x264_reference_reset( h );

        i_nal_type    = NAL_SLICE_IDR;
        i_nal_ref_idc = NAL_PRIORITY_HIGHEST;
        h->sh.i_type = SLICE_TYPE_I;
        WHEN_DEBUG_VERBOSE( fprintf(stderr, "XXX got a IDR.\n"); )

    } else if( h->fenc->i_type == X264_TYPE_I ) {
        i_nal_type    = NAL_SLICE;
        // Not completely true but for now it is (as all I/P are kept as ref)
        i_nal_ref_idc = NAL_PRIORITY_HIGH;
        h->sh.i_type = SLICE_TYPE_I;
        WHEN_DEBUG_VERBOSE( fprintf(stderr, "XXX got a I.\n"); )

    } else if( h->fenc->i_type == X264_TYPE_P ) {
        i_nal_type    = NAL_SLICE;
        // Not completely true but for now it is (as all I/P are kept as ref)
        i_nal_ref_idc = NAL_PRIORITY_HIGH; 
        h->sh.i_type = SLICE_TYPE_P;
        WHEN_DEBUG_VERBOSE( fprintf(stderr, "XXX got a P.\n"); )

    } else if( h->fenc->i_type == X264_TYPE_BREF ) {
        /* ANGE: We can't handle BREF frame ever */
        assert(0); 
        i_nal_type    = NAL_SLICE;
        i_nal_ref_idc = NAL_PRIORITY_HIGH; 
        h->sh.i_type = SLICE_TYPE_B;
        WHEN_DEBUG_VERBOSE( fprintf(stderr, "XXX got a B ref.\n"); )

    } else { /* B frame */
        /* ANGE: B-frame should be disabled for now. */
        WHEN_DEBUG( assert(0); )
        i_nal_type    = NAL_SLICE;
        i_nal_ref_idc = NAL_PRIORITY_DISPOSABLE;
        h->sh.i_type = SLICE_TYPE_B;
        WHEN_DEBUG_VERBOSE( fprintf(stderr, "XXX got a B.\n"); )

    }

    h->fdec->i_poc =
    h->fenc->i_poc = 2 * (h->fenc->i_frame - h->frames.i_last_idr);
    h->fdec->i_type = h->fenc->i_type;
    h->fdec->i_frame = h->fenc->i_frame;
    h->fenc->b_kept_as_ref =
    h->fdec->b_kept_as_ref = 
        i_nal_ref_idc != NAL_PRIORITY_DISPOSABLE && h->param.i_keyint_max > 1;

    WHEN_DEBUG_VERBOSE(
        fprintf(stderr,
                "Process h iframe: %d, fdec iframe %d poc %d, fenc iframe %d poc %d.\n",
                h->i_frame, h->fdec->i_frame, h->fdec->i_poc, h->fenc->i_frame, h->fenc->i_poc); )

    /* ------------------- Init                ----------------------------- */
    /* build ref list 0/1 */
    x264_reference_build_list( h, h->fdec->i_poc );

    WHEN_DEBUG_VERBOSE(
            fprintf(stderr,
                "Process h iframe: %d, fdec iframe %d poc %d, fenc iframe %d poc %d type %d.\n",
                h->i_frame, h->fdec->i_frame, h->fdec->i_poc,
                h->fenc->i_frame, h->fenc->i_poc, h->fenc->i_type); )
    WHEN_DEBUG_VERBOSE( print_ref_lists(h); )

    /* Init the rate control */
    x264_ratecontrol_start( h, h->fenc->i_qpplus1 );
    i_global_qp = x264_ratecontrol_qp( h );

    pic_out->i_qpplus1 =
    h->fdec->i_qpplus1 = i_global_qp + 1;

    if( h->sh.i_type == SLICE_TYPE_B )
        x264_macroblock_bipred_init( h );
    
    *pi_global_qp = i_global_qp;
    h->i_nal_type = i_nal_type;
    h->i_nal_ref_idc = i_nal_ref_idc;

    return 1;
}


static inline void 
x264_stage_start(x264_t *h, int i_global_qp) {

    /* ------------------------ Create slice header  ----------------------- */
    x264_slice_init( h, h->i_nal_type, i_global_qp );

   /* ANGE: equivalent to if( i_nal_ref_idc != NAL_PRIORITY_DISPOSABLE ) */
   if( h->fenc->i_type != X264_TYPE_B )
        h->i_frame_num++;

    /* ---------------------- Write the bitstream -------------------------- */
    /* Init bitstream context */
    h->out.i_nal = 0;
    bs_init( &h->out.bs, h->out.p_bitstream, h->out.i_bitstream );

    /* ANGE: B-frame is disabled for now. */
    WHEN_DEBUG( assert(h->sh.i_type != SLICE_TYPE_B); );

    if(h->param.b_aud){
        int pic_type;

        if(h->sh.i_type == SLICE_TYPE_I)
            pic_type = 0;
        else if(h->sh.i_type == SLICE_TYPE_P)
            pic_type = 1;
        else if(h->sh.i_type == SLICE_TYPE_B)
            pic_type = 2;
        else
            pic_type = 7;

        x264_nal_start(h, NAL_AUD, NAL_PRIORITY_DISPOSABLE);
        bs_write(&h->out.bs, 3, pic_type);
        bs_rbsp_trailing(&h->out.bs);
        x264_nal_end(h);
    }

    /* ANGE: move to end of x264_stage_init 
    h->i_nal_type = i_nal_type;
    h->i_nal_ref_idc = i_nal_ref_idc;
    */

    /* Write SPS and PPS */
    if( h->i_nal_type == NAL_SLICE_IDR && h->param.b_repeat_headers ) {
        if( h->fenc->i_frame == 0 ) {
            /* identify ourself */
            x264_nal_start( h, NAL_SEI, NAL_PRIORITY_DISPOSABLE );
            x264_sei_version_write( h, &h->out.bs );
            x264_nal_end( h );
        }

        /* generate sequence parameters */
        x264_nal_start( h, NAL_SPS, NAL_PRIORITY_HIGHEST );
        x264_sps_write( &h->out.bs, h->sps );
        x264_nal_end( h );

        /* generate picture parameters */
        x264_nal_start( h, NAL_PPS, NAL_PRIORITY_HIGHEST );
        x264_pps_write( &h->out.bs, h->pps );
        x264_nal_end( h );
    }

}

//execute right after all rows are processed
static inline int x264_stage_afterrow (x264_t *h, int i_skip) {
    if( h->param.b_cabac ) {
        x264_cabac_encode_flush( h, &h->cabac );
        h->out.bs.p = h->cabac.p;
    }
    else {
        if( i_skip > 0 )
            bs_write_ue( &h->out.bs, i_skip );  /* last skip run */
        /* rbsp_slice_trailing_bits */
        bs_rbsp_trailing( &h->out.bs );
    }
    x264_nal_end( h );

    x264_fdec_filter_row( h, h->sps->i_mb_height );

    /* Compute misc bits */
    h->stat.frame.i_misc_bits = bs_pos( &h->out.bs )
                              + NALU_OVERHEAD * 8
                              - h->stat.frame.i_tex_bits
                              - h->stat.frame.i_mv_bits;

    h->out.i_frame_size = h->out.nal[h->out.i_nal-1].i_payload;  //x264_slices_write J

    WHEN_DEBUG_VERBOSE(
        fprintf(stderr,
                "wrote h iframe: %d, fdec iframe %d poc %d, fenc iframe %d poc %d.\n",
                h->i_frame, h->fdec->i_frame, h->fdec->i_poc, h->fenc->i_frame, h->fenc->i_poc); )

    // x264_slices_write( h );

    return 1;
}

// write frame to file
static inline int64_t
x264_stage_write(x264_t *h, x264_picture_t* pic_out, hnd_t hout) {

    int i, i_nal;
    x264_nal_t *nal; 
    int64_t i_file = 0;

    /* no data out */
    i_nal = 0;
    nal = NULL;

    /* restore CPU state (before using float again) */
    x264_emms();

    // ANGE XXX: Put it back later
    // deleted reencode J 

    x264_encoder_frame_end( h, &nal, &i_nal, pic_out );

    for( i = 0; i < i_nal; i++ ) {
        int i_size;        

        if( mux_buffer_size < nal[i].i_payload * 3/2 + 4 ) {
            mux_buffer_size = nal[i].i_payload * 2 + 4;
            x264_free( mux_buffer );
            mux_buffer = (uint8_t*)x264_malloc( mux_buffer_size );
        }

        i_size = mux_buffer_size;

        x264_nal_encode( mux_buffer, &i_size, 1, &(nal[i]) ); 
        int64_t tmp = p_write_nalu( hout, mux_buffer, i_size );
        i_file += tmp;
// fprintf(stderr, "Wrote %ld.\n", tmp);
    }
    if (i_nal) {
        p_set_eop( hout, pic_out );
#if PADDING
        write_padding(h->fenc->i_frame, hout);
#endif
    }

    return i_file;
}

// execute pstage right after start
#if 0
static inline int 
x264_stage_pn(x264_t *h, int* pi_mb_x, int* pi_mb_y, int* pi_skip) {

#ifdef HAVE_MMX
    /* Misalign mask has to be set separately for each thread. */
    if( h->param.cpu&X264_CPU_SSE_MISALIGN )
        x264_cpu_mask_misalign_sse();
#endif

#if VISUALIZE
    if( h->param.b_visualize )
        x264_visualize_init( h );
#endif

    /* ANGE XXX: The rest should execute w/ aligned stack */
    /* init stats */
    memset( &h->stat.frame, 0, sizeof(h->stat.frame) );

    /* Slice */
    x264_nal_start( h, h->i_nal_type, h->i_nal_ref_idc );

    /* Slice header */
    x264_slice_header_write( &h->out.bs, &h->sh, h->i_nal_ref_idc );
    if( h->param.b_cabac ) {
        /* alignment needed */
        bs_align_1( &h->out.bs );

        /* init cabac */
        x264_cabac_context_init( &h->cabac, h->sh.i_type, h->sh.i_qp, h->sh.i_cabac_init_idc );
        /* ANGE: so fields in h->cabac simply points to that in h->out.bs */
        x264_cabac_encode_init ( &h->cabac, h->out.bs.p, h->out.bs.p_end );
    }
    h->mb.i_last_qp = h->sh.i_qp;
    h->mb.i_last_dqp = 0;

    *pi_mb_y = h->sh.i_first_mb / h->sps->i_mb_width;
    *pi_mb_x = h->sh.i_first_mb % h->sps->i_mb_width;
    *pi_skip = 0;

    return 1;
}

//execute each row
static inline int 
x264_stage_process_one_block(x264_t *h, int i_mb_x, int i_mb_y, 
        int* pi_skip, int mb_xy) {
    int i, i_list, i_ref;

    int mb_spos = bs_pos(&h->out.bs) + x264_cabac_pos(&h->cabac);

    if( i_mb_x == 0 )
        x264_fdec_filter_row( h, i_mb_y );

    /* load cache */
    x264_macroblock_cache_load( h, i_mb_x, i_mb_y );

    /* analyse parameters
     * Slice I: choose I_4x4 or I_16x16 mode
     * Slice P: choose between using P mode or intra (4x4 or 16x16)
     * */
    x264_macroblock_analyse( h );

    /* encode this macroblock -> be careful it can change the mb type to P_SKIP if needed */
    x264_macroblock_encode( h );

    x264_bitstream_check_buffer( h );

    if( h->param.b_cabac ) {
        if( mb_xy > h->sh.i_first_mb && !(h->sh.b_mbaff && ((i_mb_y)&1)) )
            x264_cabac_encode_terminal( &h->cabac );

        if( IS_SKIP( h->mb.i_type ) ) {
            x264_cabac_mb_skip( h, 1 );

        } else {
            if( h->sh.i_type != SLICE_TYPE_I )
                x264_cabac_mb_skip( h, 0 );
            x264_macroblock_write_cabac( h, &h->cabac );
        }
    } else {
        if( IS_SKIP( h->mb.i_type ) )
            (*pi_skip)++;
        else {
            if( h->sh.i_type != SLICE_TYPE_I ) {
                bs_write_ue( &h->out.bs, *pi_skip );  /* skip run */
                (*pi_skip) = 0;
            }
            x264_macroblock_write_cavlc( h, &h->out.bs );
        }
    }

#if VISUALIZE
        /* ANGE: save data from h->mb to h->visualize */
        if( h->param.b_visualize )
            x264_visualize_mb( h );
#endif

    /* save cache */
    /* ANGE: save everything from h->mb back into h->mb.cache; reverse of cache_load */
    x264_macroblock_cache_save( h );

    /* accumulate mb stats */
    h->stat.frame.i_mb_count[h->mb.i_type]++;
    if( !IS_SKIP(h->mb.i_type) && 
        !IS_INTRA(h->mb.i_type) && !IS_DIRECT(h->mb.i_type) ) {
        if( h->mb.i_partition != D_8x8 )
            h->stat.frame.i_mb_partition[h->mb.i_partition] += 4;
        else
            for( i = 0; i < 4; i++ )
                h->stat.frame.i_mb_partition[h->mb.i_sub_partition[i]] ++;
        if( h->param.i_frame_reference > 1 )
            for( i_list = 0; i_list <= (h->sh.i_type == SLICE_TYPE_B); i_list++ )
                for( i = 0; i < 4; i++ ) {
                    i_ref = h->mb.cache.ref[i_list][ x264_scan8[4*i] ];
                    if( i_ref >= 0 )
                        h->stat.frame.i_mb_count_ref[i_list][i_ref] ++;
                }
    }
    if( h->mb.i_cbp_luma && !IS_INTRA(h->mb.i_type) ) {
        h->stat.frame.i_mb_count_8x8dct[0] ++;
        h->stat.frame.i_mb_count_8x8dct[1] += h->mb.b_transform_8x8;
    }

    /* ANGE: This modifies h->rc and h->fdec->i_row_bits[h->mb.i_mb_y]; it also sets
             h->rc->frame_size_estimated.  Note the acceses this field is synchronized 
             (via locking in function calls to x264_ratecontrol_get/set_estimated_size. */
    x264_ratecontrol_mb( h, bs_pos(&h->out.bs) + x264_cabac_pos(&h->cabac) - mb_spos );

    return 1;
}
#endif


