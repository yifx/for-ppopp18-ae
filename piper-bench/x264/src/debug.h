#ifndef _DEBUG_H
#define _DEBUG_H

#define DEBUG_VERBOSE 0

#if DEBUG
#define WHEN_DEBUG(ex) ex
#else 
#define WHEN_DEBUG(ex)
#endif
        
#if DEBUG_VERBOSE
#define WHEN_DEBUG_VERBOSE(ex) ex 
#else   
#define WHEN_DEBUG_VERBOSE(ex)
#endif      

#endif
