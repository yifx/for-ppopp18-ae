package examples

import scalapipe._
import scalapipe.dsl._
import scalapipe.kernels._

object LZ77 extends App {

    val FILE = new NativeType("FILE")
    val FILE_PTR = new Pointer(FILE)
    
    val DICTSIZE = 4096
    val BUFFSIZE = 64
    
    val DICT = Vector(UNSIGNED8,DICTSIZE)
    val BUFF = Vector(UNSIGNED8,BUFFSIZE)
    
    val NITEMS = 16384
    val STR = Vector(UNSIGNED8,NITEMS+1)
//     val STR = new NativeType("strbuff",NITEMS*8)
    
    val OUT = new NativeType("struct out")
    
    class stdioFunc(_name: String) extends Func(_name)
    {
        include("stdio.h")
        external("C")
    }
    val fopen = new stdioFunc("fopen")
    {
        returns(FILE_PTR)
    }

    val fclose = new stdioFunc("fclose")
    {
        returns(SIGNED32)
    }
    
    val fwrite = new stdioFunc("fwrite")
    {
        returns(SIGNED32)
    }
    
    val fread = new stdioFunc("fread")
    {
        returns(SIGNED32)
    }
    
    val feof = new stdioFunc("feof")
    {
        returns(SIGNED32)
    }
    val printf = new stdioFunc("printf")
    {
        returns(SIGNED32)
    }
    
    val fprintf = new stdioFunc("fprintf")
    {
        returns(SIGNED32)
    }
    
    class compFunc(_name: String) extends Func(_name)
    {
        include("../lz77.h")
        objectf("../lz77.o")
        external("C")
    }
    
    val compress = new compFunc("compress")
    {
    }
    
    val decompress = new compFunc("decompress")
    {
    }
    
    val backclear = new compFunc("backclear")
    {
    }
    
    
    val comp = true
    val rawFile = "testlg.cpp"
    val compressedFile = "testlg.cmp"
    val decFile = "testlg.dec"
    val filesize = 98304000
    val ITERATIONS = (filesize+NITEMS-1) / NITEMS
    

    val FileReader = new Kernel("Reader") {

        val state = config(SIGNED32, 'state, 1306)
        val runtime = config(SIGNED32, 'runtime, 6141)
        val outrate = config(SIGNED32, 'outrate, 1)
    
        val out = output(STR,null,1)
        
        val fn = local(STRING, rawFile)
        val fd = local(FILE_PTR, 0)
        val cnt = local(SIGNED32,0)
        val instr = local(Pointer(UNSIGNED8))
        val iters = local(SIGNED32,0)
        

        if (fd == 0) {
            fd = fopen(fn, "rb")
            if (fd == 0) {
                stdio.printf("ERROR: could not open %s\n", fn)
                stdio.exit(-1)
            }
        }
        instr = addr(out(0))
        
        cnt = fread(instr,1,NITEMS,fd)

        if (cnt < NITEMS)
        {
            backclear(instr,cnt,NITEMS+1)
        }
//         printf("%s\n",instr)
        write(out)
        iters = iters + 1
        if (iters == ITERATIONS)
        {
            fclose(fd)
        }
    }

    val CompressedFileWriter = new Kernel("CmpWriter") {

        val state = config(SIGNED32, 'state, 1288)
        val runtime = config(SIGNED32, 'runtime, 3537920)
        val inrate = config(SIGNED32, 'inrate, NITEMS)
    
        val in = input(SIGNED32,null,NITEMS)
        val fn = local(STRING,compressedFile)

        val fd = local(stdio.FILEPTR, 0)
        val temp = local(SIGNED32)
        val cnt = local(UNSIGNED32)
        val clear = local(UNSIGNED32)
        val iters = local(UNSIGNED32,0)
        if (fd == 0) {
            fd = stdio.fopen(fn, "wb")
            if (fd == 0) {
                stdio.printf("ERROR: could not open %s\n", fn)
                stdio.exit(-1)
            }
        }
        cnt = 0
        clear = 0
        for( cnt <- 0 until NITEMS)
        {
            temp = in
            if (clear == 0)
            {
                fwrite(addr(temp),1,4,fd)
                if (temp < 0)
                {
                    clear = 1
                }
            }
        }
        iters = iters + 1
        if (iters == ITERATIONS)
        {
            fclose(fd)
        }
    }
    
    val CompressedFileReader = new Kernel("CmpReader") {

        val state = config(SIGNED32, 'state, 1278)
        val runtime = config(SIGNED32, 'runtime, 801358)
        val outrate = config(SIGNED32, 'outrate, NITEMS)
    
        val out = output(SIGNED32,null,NITEMS)
        val fn = local(STRING, compressedFile)
        val fd = local(FILE_PTR, 0)
        
        val cnt = local(SIGNED32)
        val clear = local(SIGNED32)
        val temp = local(SIGNED32)
        val iters = local(UNSIGNED32,0)
        if (fd == 0) {
            fd = fopen(fn, "rb")
            if (fd == 0) {
                stdio.printf("ERROR: could not open %s\n", fn)
                stdio.exit(-1)
            }
        }
        
        
        clear = 0
        for( cnt <- 0 until NITEMS)
        {
            if (clear == 0)
            {
                fread(addr(temp),1,4,fd)
                if (temp < 0)
                    clear = 1
            }
            out = temp            
        }
        iters = iters + 1
        if (iters == ITERATIONS)
            fclose(fd)

    }

    val FileWriter = new Kernel("Writer") {

        val state = config(SIGNED32, 'state, 1200)
        val runtime = config(SIGNED32, 'runtime, 58876)
        val inrate = config(SIGNED32, 'inrate, 1)
    
        val in = input(STR,null,1)
        val fn = local(STRING, decFile)
        val fd = local(FILE_PTR, 0)
        
        val iters = local(UNSIGNED32, 0)
        val temp = local(Pointer(UNSIGNED8))
        
        if (fd == 0) {
            fd = fopen(fn, "wb")
            if (fd == 0) {
                stdio.printf("ERROR: could not open %s\n", fn)
                stdio.exit(-1)
            }
        }
        read(in)
        temp = addr(in(0))
        fprintf(fd,"%s",temp)
        iters = iters + 1
        if (iters == ITERATIONS)
            fclose(fd)
    }

    val CCompress = new Kernel("CCompress",true)
    {
        val state = config(SIGNED32, 'state, 1498)
        val runtime = config(SIGNED32, 'runtime, 42381569)
        val inrate = config(SIGNED32, 'inrate, 1)
        val outrate = config(SIGNED32, 'outrate, NITEMS)
        
        val in = input(STR,null,1)
        val out = output(SIGNED32,null,NITEMS)
        
        val cnt = local(SIGNED32)
        val i = local(SIGNED32)
        val code = local(SIGNED32)
        val sptr = local(Pointer(UNSIGNED8))
        
        val dict = local(DICT)
        val buff = local(BUFF)
        val clear = local(SIGNED32)
        read(in)
        sptr = addr(in(0))
        for(i <- 0 until DICTSIZE)
        {
            dict(i) = '\0';
        }
        for(i <- 0 until BUFFSIZE)
        {
            buff(i) = ataddr(sptr);
            sptr = sptr + 1
        }
        
        clear = 0
        for(cnt <- 0 until NITEMS)
        {
            if (clear == 0)
            {
                compress(addr(sptr),addr(dict(0)),DICTSIZE,addr(buff(0)),BUFFSIZE,addr(code))
//                 printf("---\n%s\n---\n%s\n",dict,buff)
            }
            out = code
            if (buff(0) == '\0' || (code & 0xFF) == '\0')
            {
//                 printf("Clearing\n")
                clear = 1
                code = (-1 << 6+8) | (0 << 8) | ' '
            }
            
        }
    }
    
    val CDecompress = new Kernel("CDecompress",true)
    {
        val state = config(SIGNED32, 'state, 1326)
        val runtime = config(SIGNED32, 'runtime, 13006824)
        val inrate = config(SIGNED32, 'inrate, NITEMS)
        val outrate = config(SIGNED32, 'outrate, 1)
        
        val in = input(SIGNED32,null,NITEMS)
        val out = output(STR,null,1)
        
        val cnt = local(SIGNED32)
        val i = local(SIGNED32)
        val code = local(SIGNED32)
        val strptr = local(Pointer(UNSIGNED8))
        
        val bn = local(UNSIGNED32)
        val clear = local(UNSIGNED8)
        val pos = local(UNSIGNED32)
        
        val dict = local(DICT)
        val buff = local(BUFF)
        
        clear = 0
        bn = 0
        strptr = addr(out(0))
        pos = 0
        for(cnt <- 0 until NITEMS)
        {
            code = in
            if (code > 0)
                decompress(code,addr(dict(0)),DICTSIZE,addr(buff(0)),BUFFSIZE,addr(bn),strptr,addr(pos))
            else if (clear == 0)
            {
                clear = 1
                backclear(strptr,pos,NITEMS)
            }
        }
        write(out)
    }


    val app = new Application {
        param('basecpu, 0)
        param('cores,8)
        param('iterations, ITERATIONS)
        param('sched, "SegRuntimeR")
//         param('debug,2)
//         param('profileMods,true)
        define("DS",DICTSIZE.toString)
        define("BS",BUFFSIZE.toString)
        
        if (comp) {
            val input = FileReader()
            val compressed = CCompress(input)
            CompressedFileWriter(compressed)
        } else {
            val cinput = CompressedFileReader()
            val raw = CDecompress(cinput)
            FileWriter(raw)
        }
    }
    app.emit("lz77")

}

