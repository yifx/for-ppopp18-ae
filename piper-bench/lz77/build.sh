#!/bin/bash
dir=`pwd`

build()
{
    sched=$1
    nm=$2

    for P in 1 2 4 8; do 
    
        cd $dir
        sed -e "s/__P__/$P/g" -e "s/__SCHED__/$sched/g" lz77.template > lz77.scala
        sbt run
        cd lz77
        make -j 8
        mv ./proc_localhost $dir/bin/lz77-lg-$nm-$P
    done    
}

build "SegBothR" "br"
build "SegRuntimeR" "rr"
