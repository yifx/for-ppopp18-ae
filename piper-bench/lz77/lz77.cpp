#include "lz77.h"
void shift( char* b, int len, int s )
{
    for(int i=0;i<len-s;i++)
        b[i] = b[s+i];
}

void print(char * b, int len)
{
    for(int i=0;i<len;i++)
    {
        if (b[i] == '\0')
            continue;
        printf("%c",b[i]);
    }
    printf("\n");
}

void match(char * dict, int ds, char * buff,int bs, struct out * o)
{
    int i=0, j=0;
    int ti,tq,tj;
    for (int q=0;q<ds;q++)
    {
        ti = q;
        tq = q;
        tj = 0;
        for(int p=0;p<bs-1;p++)
        {
            if (dict[tq] == buff[p])
            {
                tj++;
                tq = (tq == ds-1) ? q : tq+1;
            }
            else break;
        }
        
        if (tj > j)
        {
            j = tj;
            i = ti;
        }
    }
    o->i = i;
    o->j = j;
    o->k = buff[j];
}

void compress(char ** s, char * dict,int ds, char * buff,int bs, int32_t * code)
{
    struct out o;
    match(dict,ds,buff,bs,&o);
    *code = o.i << 14 | o.j << 8 | (int)o.k;
//     printf("(%d,%d,%c)\n",o.i,o.j,o.k);
    if (o.k == '\0') 
    {
//         printf("brk\n");
        return;
    }
    // Shift dictionary
    shift(dict,ds,o.j+1);
    for(int i=0;i<o.j+1;i++)
        dict[ds-(o.j+1)+i] = buff[i];
//     print(dict,ds);
    
    shift(buff,bs,o.j+1);
    for(int i=0;i<o.j+1;i++)
    {
        
        buff[bs-(o.j+1)+i] = (*s)[i];
        if ((*s)[i] == '\0') 
            break;
    }
//     printf("buff(%d) %s\n",bs,buff);
    *s += o.j+1;
}

void decompress(int32_t code, char * dict,int ds, char * buff,int bs, uint32_t * bn, char * ostr, uint32_t * pos)
{
    // Push the buffer into the dictionary
    shift(dict,ds,*bn);
    for(int p=0;p<*bn;p++)
        dict[ds-*bn+p] = buff[p];
    
    // Pull out i,j,k
    char k = code & 0xFF;
    code >>= 8;
    int j = code & 0x3f;
    code >>= 6;
    int i = code & 0x3FFFF;
//     printf("(%d,%d,%c)\n",i,j,k);
    
    int d = i;
    char s[bs];
    for(int p=0;p<j;p++)
    {
        s[p] = dict[d];
        d = (d+1 == ds) ? i : d+1;
    }
    
//     printf("%x (%c)\n",k,k);
    s[j] = k;
    if (k!='\0') 
    {
        s[j+1] = '\0';
//         strncpy(&ostr[*pos],s,j+1);
        memcpy(&ostr[*pos],s,j);
        *pos+=j;
//         printf("a%s (%d)\n",s,j+1);
    }
    else
    {
//         strncpy(&ostr[*pos],s,j);
        memcpy(&ostr[*pos],s,j-1);
        *pos+=j-1;
//         printf("b%s (%d)\n",s,j);
    }
    for(int p=0;p<j+1;p++)
        buff[p] = s[p];
    *bn = j+1;
    
}

void backclear(char * str, int pos, int len)
{
    for(int i=pos;i<len;i++)
    {
        str[i]='\0';
    }
}