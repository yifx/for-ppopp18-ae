#ifndef __LZ77_H__
#define __LZ77_H__

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
struct out
{
    int i;
    int j;
    char k;
};

typedef char V16384_UNSIGNED8[16384];

void compress(char ** s, char * dict, int ds, char * buff, int bs, int32_t * code);
void decompress(int32_t code, char * dict, int ds, char * buff,int bs, uint32_t * bn, char * ostr, uint32_t * pos);
void backclear(char * str, int pos, int len);
#endif