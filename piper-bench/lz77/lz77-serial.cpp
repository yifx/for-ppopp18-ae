#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>

int ds;
int bs;
int cs;

char * infile;
char * otfile;
FILE * infd;
FILE * otfd;

struct out
{
    int i;
    int j;
    char k;
};

void shift( char* b, int len, int s )
{
    for(int i=0;i<len-s;i++)
        b[i] = b[s+i];
}
int print(char * b, int len)
{
    for(int i=0;i<len;i++)
        printf("%c",b[i] == '\0' ? '~' : b[i]);
    printf("\n");
}

void match(char * dict, char * buff, struct out * o)
{
    int i=0, j=0;
    int ti,tq,tj;
    for (int q=0;q<ds;q++)
    {
        ti = q;
        tq = q;
        tj = 0;
        for(int p=0;p<bs-1;p++)
        {
            if (dict[tq] == buff[p])
            {
                tj++;
                tq = (tq == ds-1) ? q : tq+1;
            }
            else break;
        }
        
        if (tj > j)
        {
            j = tj;
            i = ti;
        }
    }
    o->i = i;
    o->j = j;
    o->k = buff[j];
}

void write(FILE* fd, int i, int j, char k)
{
    uint32_t line = 0;
    line = line | (i << 6+8) | (j << 8) | (int) k;
    fwrite(&line,1,4,fd);
}

void comp_str(char * s, struct out * o)
{
    
}

void compress()
{
    char instring[cs+1];
    char * s;
    while(!feof(infd))
    {
        s = &instring[0];
        // Read in a chunk
        int r = fread(s,1,cs,infd);
        for(int i=r;i<cs+1;i++)
        {
            instring[i] = '\0';
        }
        char dict[ds];
        char buff[bs];
        for(int i=0;i<bs;i++)
            buff[i] = s[i];
        for(int i=0;i<ds;i++) dict[i] = '\0';
        s += bs;
        
        struct out o;
        while(buff[0] != '\0')
        {
            match(dict,buff,&o);
            write(otfd,o.i,o.j,o.k);
            printf("(%d,%d,%c)\n",o.i,o.j,o.k);
            // Shift dictionary
            if (o.k == '\0') {
                printf("brk\n");
                break;
            }
            
            shift(dict,ds,o.j+1);
            for(int i=0;i<o.j+1;i++)
                dict[ds-(o.j+1)+i] = buff[i];
            
            shift(buff,bs,o.j+1);
            for(int i=0;i<o.j+1;i++)
            {
                buff[bs-(o.j+1)+i] = s[i];
                if (s[i] == '\0') 
                    break;
            }
            s += o.j+1;
        }
        write(otfd,-1,0,' ');
    }
}

void decompress()
{
    int32_t line = 0;
    bool done = false;
    int bn = 0;
    int i,j;
    char k;
    char s[bs];
    while(!done)
    {
        char dict[ds];
        char buff[bs];
        for(;;)
        {
            shift(dict,ds,bn);
            for(int p=0;p<bn;p++)
                dict[ds-bn+p] = buff[p];
            fread(&line,4,1,infd);
            if (line < 0)
            {
                if (feof(infd))
                    done = true;
                break;
            }
            // Pull out i,j,k
            k = line & 0xFF;
            line >>= 8;
            j = line & 0x3f;
            line >>= 6;
            i = line & 0x3FFFF;

            int d = i;
            for(int p=0;p<j;p++)
            {
                s[p] = dict[d];
                d = (d+1 == ds) ? i : d+1;
            }
//             printf("%x (%c)\n",k,k);
            s[j] = k;
            
            if (k!='\0') 
            {
                s[j+1] = '\0';
                fwrite(s,1,j+1,otfd);
                printf("a%s (%d)\n",s,j+1);
            }
            else
            {
                fwrite(s,1,j,otfd);
                printf("b%s (%d)\n",s,j);
            }
            for(int p=0;p<j+1;p++)
                buff[p] = s[p];
            bn = j+1;
            
            ///////
            if (feof(infd))
            {
                done = true;
                break;
            }
        }
        
    }
}

int main(int argc, char* argv[])
{
    if (argc < 7)
    {
        printf("Usage : %s <comp/dec> <dictionary size> <buffer size> <chunk size> infile outfile\n", argv[0]);
        return -1;
    }
    int comp = atoi(argv[1]);
    ds = atoi(argv[2]);
    bs = atoi(argv[3]);
    cs = atoi(argv[4]);
    infile = argv[5];
    otfile = argv[6];
    
    infd = fopen(infile,"r");
    otfd = fopen(otfile,"w");
    
    if (comp == 1)
        compress();
    else
        decompress();
    
    fclose(infd);
    fclose(otfd);
    
}